<?php

namespace Drupal\shortpixel\Plugin\ImageEffect;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Url;
use Drupal\image\ConfigurableImageEffectBase;
use Drupal\imagee\Image;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Runs an image through Shortpixel.
 *
 * @ImageEffect(
 *   id = "shortpixel",
 *   label = @Translation("shortpixel"),
 *   description = @Translation("Apply the Shortpixel API to the image.")
 * )
 */
class ShortpixelEffect extends ConfigurableImageEffectBase {

  const APIURL = "https://api.shortpixel.com/v2/reducer-sync.php";

  const VERSION = 'DROP1';

  /**
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var string
   */
  protected $apiKey;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * @var array
   */
  protected $tempFiles = [];

  /**
   * ShortpixelEffect constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Psr\Log\LoggerInterface $logger
   * @param $apiKey
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, $apiKey, FileSystemInterface $fileSystem, ClientInterface $client) {
    $this->apiKey = $apiKey;
    $this->fileSystem = $fileSystem;
    $this->client = $client;
    drupal_register_shutdown_function([$this, 'removeTempFiles']);
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('image'),
      $container->get('config.factory')->get('shortpixel.settings')->get('api_key'),
      $container->get('file_system'),
      $container->get('http_client')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    $source = $image->getSource();
    $tempInput = $this->fileSystem->tempnam('temporary://', 'shortpixel');
    $this->tempFiles[] = $tempInput;
    if ($source && $image instanceof Image && $image->save($tempInput)) {
      $tempOutput = $this->fileSystem->tempnam('temporary://', 'shortpixel');
      $this->tempFiles[] = $tempOutput;
      try {
        $this->client->post(self::APIURL, [
          'sink' => $this->fileSystem->realpath($tempOutput),
          'headers' => ['Accept' => 'application/json'],
          RequestOptions::JSON => array_intersect_key($this->configuration, $this->defaultConfiguration()) + [
            'key' => $this->apiKey,
            'plugin_version' => self::VERSION,
            // shortpixel refresh: 1 times out with the sync endpoint, use a
            // unique URL instead.
            'url' => file_create_url($tempInput) . '?' . (new \Drupal\Component\Utility\Random())->word(6),
          ],
        ]);
        $image->setSource($tempOutput);
        return TRUE;
      }
      catch (\Exception $e) {
        // The image style UI dies if the exceptions are not caught.
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'lossy' => 2,
      'resize' => 0,
      'resize_width' => 0,
      'resize_height' => 0,
      'cmyk2rgb' => 1,
      'keep_exif' => 0,
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['lossy'] = [
      '#type' => 'select',
      '#title' => $this->t('Compression type'),
      '#options' => [
        0 => $this->t('Lossless'),
        1 => $this->t('Lossy'),
        2 => $this->t('Glossy'),
      ],
      '#default_value' => $this->configuration['lossy'],
    ];
    $form['resize'] = [
      '#type' => 'select',
      '#title' => $this->t('Resize'),
      '#options' => [
        0 => $this->t('No resize'),
        1 => $this->t('Outer'),
        3 => $this->t('Inner'),
      ],
      '#default_value' => $this->configuration['resize'],
    ];
    $form['resize_config'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
      '#states' => [
        'visible' => [
          ':input[name="data[resize]"]' => ['!value' => 0],
        ],
      ],
    ];
    $form['resize_config']['resize_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->configuration['resize_width'],
    ];
    $form['resize_config']['X']['#markup'] = ' X ';
    $form['resize_config']['resize_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->configuration['resize_height'],
    ];
    $form['cmyk2rgb'] = [
      '#type' => 'checkbox',
      '#title' => t('Convert CMYK to RGB?'),
      '#default_value' => $this->configuration['cmyk2rgb'],
    ];
    $form['keep_exif'] = [
      '#type' => 'checkbox',
      '#title' => t('Keep EXIF?'),
      '#default_value' => $this->configuration['keep_exif'],
    ];
    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$this->apiKey) {
      $url = Url::fromRoute('shortpixel.settings')->toString();
      $this->messenger()->addWarning(t('You do not have your API key configured, click <a href="@url">here</a> to enter it.', ['@url' => $url]));
    }
  }


  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $defaultConfiguration = $this->defaultConfiguration();
    // resize_height and weight are under resize_config so flatten values.
    foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($form_state->getValues())) as $key => $value) {
      if (isset($defaultConfiguration[$key])) {
        $this->configuration[$key] = $value;
      }
    }
  }

  public function removeTempFiles() {
    @array_walk($this->tempFiles, 'unlink');
  }

}
