<?php

namespace Drupal\shortpixel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ShortpixelSettings extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['shortpixel.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shortpixel_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('API key'),
      '#default_value' => $this->config($this->getEditableConfigNames()[0])->get('api_key'),
      '#maxlength' => 32,
      '#description' => t('Click <a href="https://shortpixel.com/wp-apikey">here</a> if you don\'t have a Shortcut account yet and <a href="https://shortpixel.com/show-api-key">here</a> if you do.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config($this->getEditableConfigNames()[0])
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
    $url = Url::fromRoute('entity.image_style.collection')->toString();
    $this->messenger()->addMessage(t('Click <a href="@url">here</a> for further configuration.', ['@url' => $url]));
    parent::submitForm($form, $form_state);
  }

}

